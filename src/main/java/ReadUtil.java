import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Sviat on 6/12/16.
 */
public class ReadUtil {
    public static String nextLine() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = "";

        try {
            input = reader.readLine();
        } catch (IOException e) {
            System.out.println("Something is wrong");
        }

        return input;
    }

    public static int nextInt() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String input = reader.readLine();

            return Integer.parseInt(input);
        } catch (Exception e) {
            System.out.println("Invalid number. Returning 0");
        }

        return 0;
    }
}