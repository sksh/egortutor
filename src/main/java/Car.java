public class Car {
	
	private String brand = "Toyota";
	
	private String model = "Corolla";
	
	private int power = 250;
	
	private Cab cab = new Cab();
	
	private Helm helm = new Helm();
	
	private Wheels wheels = new Wheels();

	public Car() {}

	public Car(String brand, String model, int power,
			String cab, String cabColor,
			String helmColor, int bottoms, int size,
			String wheelsType, int diameter, int maxLoad){
		this.brand = brand;
		this.model = model;
		this.power = power;
		this.cab = new Cab (cab, cabColor);
		this.helm = new Helm (helmColor, bottoms, size);
		this.wheels = new Wheels (maxLoad, diameter, wheelsType);
	}
	
	public Car(String brand, String model, int power, Cab cab, Helm helm, Wheels wheels) {
		super();
		this.brand = brand;
		this.model = model;
		this.power = power;
		this.cab = cab;
		this.helm = helm;
		this.wheels = wheels;
	}

	public void increasePower(){
		power += 25;
	}
	
	public void helmIncreaseSize(){
		helm.increaseSize();
	}
	
	public void moreHelmBottoms(){
		helm.moreBottoms();
	}

	public void wheelsIncreaseMaxLoad(){
		wheels.increaseMaxLoad();
	}
	
	public void wheelsIncreaseDiameter(){
		wheels.increaseDiameter();
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public Cab getCab() {
		return cab;
	}

	public void setCab(Cab cab) {
		this.cab = cab;
	}

	public Helm getHelm() {
		return helm;
	}

	public void setHelm(Helm helm) {
		this.helm = helm;
	}

	public Wheels getWheels() {
		return wheels;
	}

	public void setWheels(Wheels wheels) {
		this.wheels = wheels;
	}

	@Override
	public String toString() {
		return "Car [brand=" + brand + ", model=" + model + ", power=" + power + ", cab=" + cab + ", helm=" + helm
				+ ", wheels=" + wheels + "]";
	}

}
