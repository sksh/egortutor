public class Cab {
	
	private String cab = "sedan";
	
	private String cabColor = "red";

	public Cab() {}

	public Cab(String cab, String cabColor) {
		this.cab = cab;
		this.cabColor = cabColor;
	}
	
	public String getCab() {
		return cab;
	}

	public void setCab(String cab) {
		this.cab = cab;
	}
	
	public String getCabColor() {
		return cabColor;
	}

	public void setCabColor(String cabColor) {
		this.cabColor = cabColor;
	}

	@Override
	public String toString() {
		return "Cab [cab=" + cab + ", cabColor=" + cabColor + "]";
	}
	
}
