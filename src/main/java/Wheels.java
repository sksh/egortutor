public class Wheels {
	
	private int diameter = 40;
	
	private int maxLoad = 500;
	
	private String wheelsType = "Summer";

	public Wheels() {}

	public Wheels(int diameter, int maxLoad, String wheelsType) {
		this.diameter = diameter;
		this.maxLoad = maxLoad;
		this.wheelsType = wheelsType;
	}
	
	public void increaseMaxLoad(){ 
		maxLoad += 50;
	}
	
	public void increaseDiameter(){ 
		diameter += 5;
	}

	public String getWheelsType() {
		return wheelsType;
	}

	public void setWheelsType(String wheelsType) {
		this.wheelsType = wheelsType;
	}

	public int getDiameter() {
		return diameter;
	}

	public void setDiameter(int diameter) {
		this.diameter = diameter;
	}

	public int getMaxLoad() {
		return maxLoad;
	}

	public void setMaxLoad(int maxLoad) {
		this.maxLoad = maxLoad;
	}

	@Override
	public String toString() {
		return "Wheels [diameter=" + diameter + ", maxLoad=" + maxLoad + ", wheelsType=" + wheelsType + "]";
	}

	

}
