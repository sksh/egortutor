public class Helm {
	
	private String helmColor = "black";
	
	private int bottoms = 4;
	
	private int size = 30;

	public Helm() {}

	public Helm(String helmColor, int bottoms, int size) {
		this.helmColor = helmColor;
		this.bottoms = bottoms;
		this.size = size;
	}
	
	public void moreBottoms(){
		bottoms ++;
	}
	
	public void increaseSize(){
		size ++ ;
	}

	public String getHelmColor() {
		return helmColor;
	}

	public void setHelmColor(String helmColor) {
		this.helmColor = helmColor;
	}

	public int getBottoms() {
		return bottoms;
	}

	public void setBottoms(int bottoms) {
		this.bottoms = bottoms;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "Helm [helmColor=" + helmColor + ", bottoms=" + bottoms + ", size=" + size + "]";
	}	
	
}
