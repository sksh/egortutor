import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Scanner sc = new Scanner(System.in);

        Car car = new Car();
        Car car2 = new Car("Porsche", "911", 450, "cabriolet", "black", "yellow", 3, 25,
                "summer", 500, 45);
        Car car3 = new Car("Mercedes Benz", "S 500", 400, "sedan", "grey", "white", 6, 25,
                "winter", 520, 45);
        Car car4 = new Car("BMW", "X6", 420, "SUV", "red", "white", 4, 30,
                "winter", 600, 50);
        Car car5 = new Car("Ferrari", "Enzo", 500, "cabriolet", "yellow", "white", 9, 25,
                "summer", 520, 45);
        Car car6 = new Car("Lamborghini", "Aventador", 490, "cabriolet", "grey", "white", 8, 25,
                "summer", 490, 45);
        Car car7 = new Car("Lexus", "S 500", 330, "coupé", "silver", "white", 7, 25,
                "winter", 510, 45);
        Car car8 = new Car("Bugatti", "Veyron", 550, "coupé", "yellow", "white", 10, 25,
                "summer", 530, 45);
        Car car9 = new Car("ZAZ", "Lanos", 120, "sedan", "black", "white", 0, 25,
                "winter", 350, 40);
        Car car10 = new Car("Lada", "1500", 140, "sedan", "white", "white", 0, 25,
                "summer", 320, 40);

        List<Car> cars = new ArrayList<>();
        cars.add(car);
        cars.add(car2);
        cars.add(car3);
        cars.add(car4);
        cars.add(car5);
        cars.add(car6);
        cars.add(car7);
        cars.add(car8);
        cars.add(car9);
        cars.add(car10);

        switchMenu(cars);
    }

    static void switchMenu(List<Car> cars) {
        while (true) {
            System.out.println("Натисніть 1 для пошуку машин за діаметром коліс");
            System.out.println("Натисніть 2 для пошуку машин за діаметром коліс та кольором кузова");
            System.out.println("Натисніть 3 для заміни розміру керма в машин червоного кольору");
            System.out.println("Натисніть 4 для збільшення діаметру коліс вдвічі для машин, кермо яких має кнопочки");
            System.out.println("Натисніть 5 для заміни машин на нові, які мають діаметр менший за введений");
            System.out.println("Натисніть 6 для пошуку машин за типом кузова");
            System.out.println("Натисніть 7 для видалення машин, які мають введений колір кузова");
            System.out.println("Натисніть 8 для заміни усім машинам, діаметр коліс яких лежить у введеному діапазоні шин на зимові");
            System.out.println("Натисніть 9 для видалення усіх машин з введеним типом кузова та введеним діапазоном розміру керма");
            System.out.println("Натисніть 10 для виведення індексу машин з введеним типом кузова");
            System.out.println("Натисніть 11 для збільшення потужності усіх машин");
            System.out.println("Натисніть 12 для збільшення розміру керма усіх машин");
            System.out.println("Натисніть 13 для збільшення кількості кнопочок на кермі у всіх машинах");
            System.out.println("Натисніть 14 для збільшення максимального навантаження на колеса усіх машин");
            System.out.println("Натисніть 15 для збільшення діаметру колес усіх машин");
            System.out.println("Натисніть 0 для виходу");

            int choice = ReadUtil.nextInt();

            switch (choice) {
                case 1: {
                    //тут завжди повертався true. Знак оклику спереді це значить НЕ.
                    if (!findWheelsSize(cars)) {
                        System.out.println("Машин з таким діаметром коліс немає");
                    } else {
                        findWheelsSize(cars);
                    }
                    break;
                }
                case 2: { //не працює
                    findWheelsSizeAndCabColor(cars);
                    break;
                }
                case 3: {
                    System.out.println("Введіть розмір керма, на який хочете поміняти в машин червоного кольору:");
                    int s = ReadUtil.nextInt();
                    printCars("Перед\n", cars);
                    changeHelmSize(cars, s);
                    printCars("Після\n", cars);
                    break;
                }
                case 4: {
                    printCars("Перед\n", cars);
                    changeWheelsSize(cars);
                    printCars("Після\n", cars);
                    break;
                }
                case 5: { //не працює
                    System.out.println("Введіть діаметр коліс, менше за який машини мають замінитись на нові:");
                    int d3 = ReadUtil.nextInt();
                    printCars("Перед\n", cars);
                    changeCars(cars, d3);
                    printCars("Після\n", cars);
                    break;
                }
                case 6: { //не працює
                    System.out.println("Введіть машини з яким типом кузова треба знайти:");
                    String cab = ReadUtil.nextLine();
                    findCab(cars, cab);
                    break;
                }
                case 7: { //не працює
                    System.out.println("Введіть колір кузова машинн, яких треба видалити");
                    String cabColor = ReadUtil.nextLine();
                    printCars("Перед\n", cars);
                    removeCarsForCabColor(cars, cabColor);
                    printCars("Після\n", cars);
                    break;
                }
                case 8: {
                    System.out.println("Введіть діапазон діаметру коліс машин, яким треба замінити шини на зимові (від - до):");
                    int d4 = ReadUtil.nextInt();
                    int d5 = ReadUtil.nextInt();
                    printCars("Перед\n", cars);
                    changeWheelsType(cars, d4, d5);
                    printCars("Після\n", cars);
                    break;
                }
                case 9: { //не працює
                    System.out.println("Введіть тип кузова машин, які треба видалити:");
                    String cab2 = ReadUtil.nextLine();
                    System.out.println("Введіть діапазон керма машин, які треба видалити (від - до):");
                    int helmd1 = ReadUtil.nextInt();
                    int helmd2 = ReadUtil.nextInt();
                    printCars("Перед\n", cars);
                    removeCarsForCabAndHekmSize(cars, cab2, helmd1, helmd2);
                    printCars("Після\n", cars);
                    break;
                }
                case 10: {
                    System.out.println("Введіть тип кузова:");
                    if (!rememberCarsIndex(cars)) {
                        System.out.println("Такого типу кузова не має");
                    } else {
                        rememberCarsIndex(cars);
                    }
                    break;
                }
                case 11: {
                    printCars("Перед\n", cars);
                    increasePower(cars);
                    printCars("Після\n", cars);
                    break;
                }
                case 12: {
                    printCars("Перед\n", cars);
                    helmIncreaseSize(cars);
                    printCars("Після\n", cars);
                    break;
                }
                case 13: {
                    printCars("Перед\n", cars);
                    moreHelmBottoms(cars);
                    printCars("Після\n", cars);
                    break;
                }
                case 14: {
                    printCars("Перед\n", cars);
                    wheelsIncreaseMaxLoad(cars);
                    printCars("Після\n", cars);
                    break;
                }
                case 15: {
                    printCars("Перед\n", cars);
                    wheelsIncreaseDiameter(cars);
                    printCars("Після\n", cars);
                    break;
                }
                case 0: {
                    System.exit(0);
                    break;
                }
                default:
                    break;
            }
        }
    }

    static boolean findWheelsSize(List<Car> cars) {
        System.out.println("Введіть діаметр коліс:");
        int d = ReadUtil.nextInt();

        //нам треба якось знати, чи ми щось знайшли. Використаємо прапорець і поставимо йому значення false.

        boolean flag = false;
        for (Car car : cars) {
            if (car.getWheels().getDiameter() == d) {
                System.out.println(car);

                //як тільки ми щось знайшли, ставимо прапорець в true
                flag = true;
            }
        }

        //вертаємо цей прапорець, якщо ми нічого не знайшли, то він ніколи не буде встановленим у true, тобто матиме
        //своє початкове значення false. Аналогічно буде і в інших методах
        return flag;
    }

    static boolean findWheelsSizeAndCabColor(List<Car> cars) {
        System.out.println("Введіть діаметр коліс:");
        int d = ReadUtil.nextInt();
        System.out.println("Введіть колір кузова:");
        String color = ReadUtil.nextLine();

        for (Car car : cars) {
            if (car.getWheels().getDiameter() == d && car.getCab().getCabColor().equals(color)) {
                System.out.println(car);
            }
        }
        return true;
    }

    static void changeHelmSize(List<Car> cars, int s) {
        for (Car car : cars) {
            if (car.getCab().getCabColor().equals("red")) {
                car.getHelm().setSize(s);
            }
        }
    }

    static void changeWheelsSize(List<Car> cars) {
        for (Car car : cars) {
            if (car.getHelm().getBottoms() > 0) {
                car.getWheels().setDiameter(car.getWheels().getDiameter() * 2);
            }
        }
    }

    // тут ти рухався в правильному напрямку, перевірив умову, створив нову машину, але нікуди не записав результат.
    // треба пройтись в циклі по колекції і перезаписати в потрібному місці, якщо умова виконалась.
    static void changeCars(List<Car> cars, int d3) {
        for (int i = 0; i < cars.size(); i++) {
            if (cars.get(i).getWheels().getDiameter() < d3) {
                cars.set(i, new Car());
            }
        }
    }

    static void findCab(List<Car> cars, String cab) {
        for (Car car : cars) {
            if (car.getCab().getCab().equals(cab)) {
                System.out.println(car);
            }
        }
    }

    static void removeCarsForCabColor(List<Car> cars, String cabColor) {
        Iterator<Car> iter = cars.iterator();
        while (iter.hasNext()) {
            Car tmp = iter.next();
            if (tmp.getCab().getCabColor().equals(cabColor)) {
                iter.remove();
            }
        }
    }

    static void changeWheelsType(List<Car> cars, int d4, int d5) {
        for (Car car : cars) {
            if (car.getWheels().getDiameter() >= d4 && car.getWheels().getDiameter() <= d5) {
                car.getWheels().setWheelsType("winter");
            }
        }
    }

    static void removeCarsForCabAndHekmSize(List<Car> cars, String cab2, int helmd1, int helmd2) {
        Iterator<Car> iter = cars.iterator();
        while (iter.hasNext()) {
            Car tmp = iter.next();
            if (tmp.getCab().getCab().equals(cab2) && tmp.getHelm().getSize() >= helmd1 && tmp.getHelm().getSize() <= helmd2) {
                iter.remove();
            }
        }
    }

    static boolean rememberCarsIndex(List<Car> cars) {
        String cab = ReadUtil.nextLine();
        for (Car car : cars) {
            if (car.getCab().getCab().equals(cab)) {
                System.out.println(cars.indexOf(car));
            }
        }
        return true;
    }

    static void increasePower(List<Car> cars) {
        for (Car car : cars) {
            car.increasePower();
        }
    }

    static void helmIncreaseSize(List<Car> cars) {
        for (Car car : cars) {
            car.helmIncreaseSize();
        }
    }

    static void moreHelmBottoms(List<Car> cars) {
        for (Car car : cars) {
            car.moreHelmBottoms();
        }
    }

    static void wheelsIncreaseMaxLoad(List<Car> cars) {
        for (Car car : cars) {
            car.wheelsIncreaseMaxLoad();
        }
    }

    static void wheelsIncreaseDiameter(List<Car> cars) {
        for (Car car : cars) {
            car.wheelsIncreaseDiameter();
        }
    }

    static void printCars(String title, List<Car> cars) {
        System.out.println(title);
        for (Car car : cars) {
            System.out.println(car);
        }
        System.out.println("Car count: " + cars.size());
    }
}
